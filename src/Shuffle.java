import java.io.File;
import java.util.HashMap;

/**
 * Created by David on 2014-03-09.
 */
public class Shuffle {
    public static void main(String[] args) {
        try {
            File file = new File("../ItuneShuffle");

            HashMap<String, File> map = new HashMap<String, File>();
            if (file.isDirectory()) {
                for (String s : file.list()) {
                    if (!s.equals(".DS_Store")) {
                        File child = new File(file.getCanonicalPath() + "/" + s);

                        if (!child.exists())
                            throw new Exception("fichier " + child.getName() + "Existe pas");
                        map.put(s, child);
                    }

                }
            }
            int i = 0;
            for (String fileName : map.keySet()) {
                File file1 = new File(file.getCanonicalPath() + "/" + fileName);
                if (!file1.renameTo(new File(file.getCanonicalPath() + "/" + i + "-" + file1.getName())))
                    throw new Exception("Pas capable de renommer");
                i++;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }

    }

}
